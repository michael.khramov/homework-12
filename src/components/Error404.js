import { Alert } from 'react-bootstrap';

function Error404() {
  return (
    <Alert variant="danger">
      Page not found
    </Alert>
  );
}

export default Error404;
