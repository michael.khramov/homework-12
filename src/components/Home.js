import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Redirect } from "react-router-dom";
import { selectLogged, selectCurrent, logout } from '../features/authSlice';
import { Button } from 'react-bootstrap';

function Home() {
  const dispatch = useDispatch();
  const logged = useSelector(selectLogged);
  const currentUser = useSelector(selectCurrent);
  console.log("logged: " + logged);

  if (logged === undefined || logged === false)
    return <Redirect to="/Login" />

  function doLogOut() {
    dispatch(logout());
  }

  return (
    <div>
      <h1>Hello {currentUser.fullName}</h1>
      <Button variant="primary" type="submit" onClick={doLogOut}>
      Log out
      </Button>
    </div>
  );
}

export default Home;