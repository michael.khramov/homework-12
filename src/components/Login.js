import { Button, Form, Alert } from 'react-bootstrap';
import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Redirect } from "react-router-dom";
import { login, setError, selectError, selectLogged, selectUsers } from '../features/authSlice';

function Login() {
  const dispatch = useDispatch();
  const logged = useSelector(selectLogged);
  const users = useSelector(selectUsers);
  const error = useSelector(selectError);

  const [email, setEmail] = useState('');
  const emailValue = email;

  const [password, setPassword] = useState('');
  const passwordValue = password; 

  if (logged === true)
    return <Redirect to="/" />

  function doLogin(e) {
    e.preventDefault();

    var user = users.find((element) => {
      return element.email === emailValue && element.password === passwordValue;
    });

    if (user === undefined)
      dispatch(setError("Such user not found"));
    else
      dispatch(login(user));
    
    return false;
  }

  return (
    <div>
      {error ? <Alert variant="danger">{error} </Alert> : ''}

      <Form onSubmit={doLogin}>
        <Form.Group className="mb-3" controlId="formEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control type="email" placeholder="Enter email" onChange={(e) => setEmail(e.target.value)} />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formPasword">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" placeholder="Password" onChange={(e) => setPassword(e.target.value)} />
        </Form.Group>

        <Button variant="primary" type="submit">
          Submit
        </Button>
      </Form>

    </div>
  );
}

export default Login;
