import './App.css';
import {
  BrowserRouter,
  Switch,
  Route,
  Link,
  Redirect
} from "react-router-dom";

import Login from './components/Login';
import Register from './components/Register';
import Home from './components/Home';
import Error404 from './components/Error404';
import {Container} from 'react-bootstrap';


function App() {
  return (
    <BrowserRouter>
     <Container className="p-3">
      <div>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/login">Login</Link>
          </li>
          <li>
            <Link to="/register">Register</Link>
          </li>          
        </ul>
        
        <hr />

        <Switch>
          <Route exact path="/" component={Home}/>
          <Route path="/login" component={Login}/>
          <Route path="/register" component={Register}/>          
          <Route path="/404" component={Error404}/>
          <Redirect to="/404" />
        </Switch>
      </div>
      </Container>
    </BrowserRouter>
  );
}

export default App;
