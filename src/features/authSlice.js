import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  logged: false,
  current: null,
  error: "",
  users: [
    { email: '11@11.11', password: '1qaz@WSX', fullName: 'Test user' }
  ]
};

export const accountSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    login: (state, action) => {
      state.logged = true;
      state.current = action.payload;
      state.error = "";
    },
    logout: (state) => {
      state.logged = false;
      state.error = "";
    },
    register: (state, action) => {
      state.logged = true;
      state.current = action.payload;
      state.users = [...state.users, action.payload];
      state.error = "";
    },
    setError: (state, action) => {      
      state.error = action.payload;
    },
  }
});

export const { login, logout, register, setError } = accountSlice.actions;

export const selectLogged = (state) => state.auth.logged;
export const selectCurrent = (state) => state.auth.current;
export const selectUsers = (state) => state.auth.users;
export const selectError = (state) => state.auth.error;

export default accountSlice.reducer;